const express = require('express');
const router = express.Router();

const controladorProductos = require('./router_productos');
const controladorClientes = require('./router_clientes');
const controladorPedidos = require('./router_pedidos');
const controladorEmpleados = require('./router_empleados');
const controladorPagos = require('./router_pagos')
const controladorCategorias = require('./router_categorias');

router.use("/productos",controladorProductos);
router.use("/clientes",controladorClientes);
router.use("/pedidos",controladorPedidos);
router.use("/empleados",controladorEmpleados);
router.use("/pagos",controladorPagos);
router.use("/categorias",controladorCategorias);

module.exports = router



