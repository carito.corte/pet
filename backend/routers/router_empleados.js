const express = require('express');
const router = express.Router();
const controladorEmpleados = require('../controllers/controller_empleados');

router.get("/listar",controladorEmpleados);
router.get("/cargar/:id",controladorEmpleados);
router.post("/agregar",controladorEmpleados);
router.post("/editar/:id",controladorEmpleados);
router.delete("/borrar/:id",controladorEmpleados);

module.exports = router;
