const express = require('express');
const router = express.Router();
const controladorPagos = require('../controllers/controller_pagos');

router.get("/listar",controladorPagos);
router.get("/cargar/:id",controladorPagos);
router.post("/agregar",controladorPagos);
router.post("/editar/:id",controladorPagos);
router.delete("/borrar/:id",controladorPagos);


module.exports = router

