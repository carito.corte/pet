const express = require('express');
const router = express.Router();
const modeloPedido = require('../models/model_pedidos');

//METODO <<SUGERIDO>> POR EL PROFESOR
//Cargar un producto http://localhost:5000/api/productos/listar
router.get('/listar', (req, res) => {
    modeloPedido.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/agregar/
router.post('/agregar', (req, res) => {
    
    const nuevoPedido = new modeloPedido({
            id: req.body.id,
            numero_orden : req.body.numero_orden,
            fecha_orden : req.body.fecha_orden,
            detalles_orden : req.body.detalles_orden,
            cantidad_producto : req.body.cantidad_producto,
            id_clientes : req.body.id_clientes,
            id_productos : req.body.id_productos,

    })
    //nuevoProducto._id = new mongoose.Types.ObjectId();
    //nuevoProducto.set('versionKey', false);
    nuevoPedido.save(function(err)
    {
        if(!err)
        {
            res.send('El pedido fue agregado exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un producto http://localhost:5000/api/productos/cargar/1
router.get('/cargar/:id', (req, res) => {
    modeloPedido.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/editar/2
router.post('/editar/:id', (req, res) => {
    modeloPedido.findOneAndUpdate(
        {id:req.params.id}
        ,{
            numero_orden : req.body.numero_orden,
            fecha_orden : req.body.fecha_orden,
            detalles_orden : req.body.detalles_orden,
            cantidad_producto : req.body.cantidad_producto,
            id_clientes : req.body.id_clientes,
            id_productos : req.body.id_productos,
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El pedido se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/pedidos/borrar/3
router.delete('/borrar/:id', (req, res) => {
    modeloPedido.findOneAndDelete(
        {id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El pedido se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router
