const express = require('express');
const router = express.Router();
const modeloCliente = require('../models/model_clientes');

//METODO <<SUGERIDO>> POR EL PROFESOR
//Cargar un cliente http://localhost:5000/api/clientes/listar
router.get('/listar', (req, res) => {
    modeloCliente.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});


//http://localhost:5000/api/clientes/agregar/
router.post('/agregar', (req, res) => {
    
    const nuevoCliente = new modeloCliente({
        id:req.body.id,
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        numero_cliente: req.body.numero_cliente,
        direccion_cliente: req.body.direccion_cliente,
        id_empleados: req.body.id_empleados
    })
    
    nuevoCliente.save(function(err)
    {
        if(!err)
        {
            res.send('El cliente fue agregado exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un cliente http://localhost:5000/api/clientes/cargar/1
router.get('/cargar/:id', (req, res) => {
    modeloCliente.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/clientes/editar/2
router.post('/editar/:id', (req, res) => {
    modeloCliente.findOneAndUpdate(
        {id:req.params.id}
        ,{
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            numero_cliente: req.body.numero_cliente,
            direccion_cliente: req.body.direccion_cliente,
            id_empleados: req.body.id_empleados
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El cliente se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/clientes/borrar/3
router.delete('/borrar/:id', (req, res) => {
    modeloCliente.findOneAndDelete(
        {id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El cliente se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router