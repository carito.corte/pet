const express = require('express');
const router = express.Router();
const modeloEmpleado = require('../models/model_empleados');

//METODO <<SUGERIDO>> POR EL PROFESOR
//Cargar un cliente http://localhost:5000/api/empleados/listar
router.get('/listar', (req, res) => {
    modeloEmpleado.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});


//http://localhost:5000/api/empleados/agregar/
router.post('/agregar', (req, res) => {
    
    const nuevoEmpleado= new modeloEmpleado({
        id:req.body.id,
        nombre_empleado: req.body.nombre_empleado,
        apellido_empleado: req.body.apellido_empleado,
        cedula_empleado: req.body.cedula_empleado,
    })
    
    nuevoEmpleado.save(function(err)
    {
        if(!err)
        {
            res.send('El empleado fue agregado exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un cliente http://localhost:5000/api/empleados/cargar/1
router.get('/cargar/:id', (req, res) => {
    modeloEmpleado.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/empleados/editar/2
router.post('/editar/:id', (req, res) => {
    modeloEmpleado.findOneAndUpdate(
        {id:req.params.id}
        ,{
            nombre_empleado: req.body.nombre_empleado,
            apellido_empleado: req.body.apellido_empleado,
            cedula_empleado: req.body.cedula_empleado,
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El empleado se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/empleados/borrar/3
router.delete('/borrar/:id', (req, res) => {
    modeloEmpleado.findOneAndDelete(
        {id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El empleado se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router