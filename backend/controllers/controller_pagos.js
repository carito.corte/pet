const express = require('express');
const router = express.Router();
const modeloPago = require('../models/model_pagos');

//METODO <<SUGERIDO>> POR EL PROFESOR
//Cargar un cliente http://localhost:5000/api/pagos/listar
router.get('/listar', (req, res) => {
    modeloPago.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});


//http://localhost:5000/api/pagos/agregar
router.post('/agregar', (req, res) => {
    
    const nuevoPago= new modeloPago({
        id:req.body.id,
        precio_total:req.body.precio_total,
        id_cliente:req.body.id_cliente,
        id_pedido:req.body.id_pedido,
    })
    
    nuevoPago.save(function(err)
    {
        if(!err)
        {
            res.send('El pago fue agregado exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un cliente http://localhost:5000/api/pagos/cargar/1
router.get('/cargar/:id', (req, res) => {
    modeloPago.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/pagos/editar/2
router.post('/editar/:id', (req, res) => {
    modeloPago.findOneAndUpdate(
        {id:req.params.id}
        ,{
            precio_total:req.body.precio_total,
            id_cliente:req.body.id_cliente,
            id_pedido:req.body.id_pedido,
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El pago se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/pagos/borrar/3
router.delete('/borrar/:id', (req, res) => {
    modeloPago.findOneAndDelete(
        {id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El pago se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router