//import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
//import pagina inicio
import PaginaPrincipal from './componentes/paginaprincipal';
//importar archivos listar
import ProductosListar from './componentes/listar/productoslistar';
import PedidosListar from './componentes/listar/pedidoslistar';
import CategoriaListar from './componentes/listar/categoriaslistar';
import ClientesListar from './componentes/listar/clienteslistar';
import EmpleadosListar from './componentes/listar/empleadoslistar';
import PagosListar from './componentes/listar/pagoslistar';

//importar archivos editar
import ProductosEditar from './componentes/editar/productoseditar';
import PedidosEditar from './componentes/editar/pedidoseditar';
import PagosEditar from './componentes/editar/pagoseditar';
import ClientesEditar from './componentes/editar/clienteseditar';
import CategoriasEditar from './componentes/editar/categoriaseditar';
import EmpleadosEditar from './componentes/editar/empleadoseditar';

//importar archivos borrar
import ProductosBorrar from './componentes/borrar/productosborrar';
import PedidosBorrar from './componentes/borrar/pedidosborrar';
import PagosBorrar from './componentes/borrar/pagosborrar';
import EmpleadosBorrar from './componentes/borrar/empleadosborrar';
import ClientesBorrar from './componentes/borrar/clientesborrar';
import CategoriasBorrar from './componentes/borrar/categoriasborrar';

//importar archivos agregar
import ProductosAgregar from './componentes/agregar/productosagregar';
import PedidosAgregar from './componentes/agregar/pedidosagregar';
import PagosAgregar from './componentes/agregar/pagosagregar';
import EmpleadosAgregar from './componentes/agregar/empleadosagregar';
import ClientesAgregar from './componentes/agregar/clientesagregar';
import CategoriasAgregar from './componentes/agregar/categoriasagregar';





function App() {
  return (
    <div className="App">
      <nav className="navbar navbar-expand-lg bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">OmegaPetShop</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li key="inicio" className="nav-item">
                <a className="nav-link active" aria-current="page" href="/">Inicio</a>
              </li>
              <li key="productos" className="nav-item">
                <a className="nav-link" href="/productoslistar">Productos</a>
              </li>
              <li key="categorias" className="nav-item">
                <a className="nav-link" href="/categoriaslistar">Categorias</a>
              </li>
              <li key="clients" className="nav-item">
                <a className="nav-link" href="/clienteslistar">Clientes</a>
              </li>
              <li key="empleados" className="nav-item">
                <a className="nav-link" href="/empleadoslistar">Empleados</a>
              </li>
              <li key="pedidos" className="nav-item">
                <a className="nav-link" href="/pedidoslistar">Pedidos</a>
              </li>
              <li key="pagos" className="nav-item">
                <a className="nav-link" href="/pagoslistar">Pagos</a>
              </li>
              
            </ul>
          </div>
        </div>
      </nav>
      <BrowserRouter>
        <Routes>
            <Route path='/' element={<PaginaPrincipal/>} exact></Route>
        listar
            <Route path='/productoslistar' element={<ProductosListar/>} exact></Route>
            <Route path='/categoriaslistar' element={<CategoriaListar/>} exact></Route>
            <Route path='/empleadoslistar' element={<EmpleadosListar/>} exact></Route>
            <Route path='/pedidoslistar' element={<PedidosListar/>} exact></Route>
            <Route path='/clienteslistar' element={<ClientesListar/>} exact></Route>
            <Route path='/pagoslistar' element={<PagosListar/>} exact></Route>
          editar
            <Route path='/productoseditar/:id' element={<ProductosEditar/>} exact></Route>
            <Route path='/clienteseditar/:id' element={<ClientesEditar/>} exact></Route>
            <Route path='/pedidoseditar/:id' element={<PedidosEditar/>} exact></Route>
            <Route path='/pagoseditar/:id' element={<PagosEditar/>} exact></Route>
            <Route path='/categoriaseditar/:id' element={<CategoriasEditar/>} exact></Route>
            <Route path='/empleadoseditar/:id' element={<EmpleadosEditar/>} exact></Route>
          agregar
            <Route path='/productosagregar' element={<ProductosAgregar/>} exact></Route>
            <Route path='/pedidosagregar' element={<PedidosAgregar/>} exact></Route>
            <Route path='/pagosagregar' element={<PagosAgregar/>} exact></Route>
            <Route path='/empleadosagregar' element={<EmpleadosAgregar/>} exact></Route>
            <Route path='/clientesagregar' element={<ClientesAgregar/>} exact></Route>
            <Route path='/categoriasagregar' element={<CategoriasAgregar/>} exact></Route>
          borrar
            <Route path='/productosborrar' element={<ProductosBorrar/>} exact></Route>
            <Route path='/pedidosborrar' element={<PedidosBorrar/>} exact></Route>
            <Route path='/pagosborrar' element={<PagosBorrar/>} exact></Route>
            <Route path='/empleadosborrar' element={<EmpleadosBorrar/>} exact></Route>
            <Route path='/clientesborrar' element={<ClientesBorrar/>} exact></Route>
            <Route path='/categoriasborrar' element={<CategoriasBorrar/>} exact></Route>
        </Routes>
      </BrowserRouter>    
    </div>
  );
}

export default App;
