import axios from 'axios';
import uniquid from 'uniqid';
import React, {useState} from 'react';
import {useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function CategoriasAgregar()
{
   
    const[descripcion,setDescripcion] = useState('')
    const[imagen,setImagen] = useState('')
    
    const navegar = useNavigate()

    function categoriasInsertar()
    {
        const categoriainsertar = {
        id: uniquid(),
        descripcion:descripcion,
        imagen:imagen
        }
    
        console.log(categoriainsertar)

        axios.post(`/api/categorias/agregar`,categoriainsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/categoriaslistar')
        }).catch(err => {console.log(err.stack)})

    }

    function categoriasRegresar()
    {
        navegar('/categoriaslistar')
    }

return(
    <div className='container mt-5'>
        <h4>Categoria</h4>
        <div className='row'>
            <div className='col-md-12'>
                <div className="mb-3">
                    <label htmlFor="descripcion" className="form-label">Descripcion</label>
                    <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e)=>{setDescripcion(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="imagen" className="form-label">Imagen</label>
                    <input type="text" className="form-control" id="imagen" value={imagen} onChange={(e)=>{setImagen(e.target.value)}}></input>
                </div>
                

                <button type="button" className="btn btn-info" onClick={categoriasRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={categoriasInsertar}>Actualizar</button>
            </div>
        </div>
    </div>
)

}

export default CategoriasAgregar;