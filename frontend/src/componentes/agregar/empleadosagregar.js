import axios from 'axios';
import uniquid from 'uniqid';
import React, {useState} from 'react';
import {useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function EmpleadosAgregar()
{
    const[nombre_empleado,setNombre_Empleado] = useState('')
    const[apellido_empleado,setApellido_Empleado] = useState('')
    const[cedula_empleado,setCedula_Empleado] = useState('')
    const navegar = useNavigate()

    function empleadosInsertar()
    {
        const empleadoinsertar = {
        id: uniquid(),
        nombre_empleado: nombre_empleado,
        apellido_empleado: apellido_empleado,
        cedula_empleado: cedula_empleado
        }
    
        console.log(empleadoinsertar)

        axios.post(`/api/empleados/agregar`,empleadoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/empleadoslistar')
        }).catch(err => {console.log(err.stack)})

    }

    function empleadosRegresar()
    {
        navegar('/empleadoslistar')
    }

return(
    <div className='container mt-5'>
        <h4>Empleado</h4>
        <div className='row'>
            <div className='col-md-12'>
            <div className="mb-3">
                    <label htmlFor="nombre_empleado" className="form-label">Nombre_Empleado</label>
                    <input type="text" className="form-control" id="nombre_empleado" value={nombre_empleado} onChange={(e)=>{setNombre_Empleado(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="apellido_empleado" className="form-label">Apellido_Empleado</label>
                    <input type="text" className="form-control" id="apellido_empleado" value={apellido_empleado} onChange={(e)=>{setApellido_Empleado(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="cedula_empleado" className="form-label">Cedula_Empleado</label>
                    <input type="text" className="form-control" id="cedula_empleado" value={cedula_empleado} onChange={(e)=>{setCedula_Empleado(e.target.value)}}></input>
                </div>
                

                <button type="button" className="btn btn-info" onClick={empleadosRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={empleadosInsertar}>Actualizar</button>
            </div>
        </div>
    </div>
)

}

export default EmpleadosAgregar;