import axios from 'axios';
import uniquid from 'uniqid';
import React, {useState} from 'react';
import {useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function PedidosAgregar()
{
    const[numero_orden,setNumero_Orden] = useState('')
    const[fecha_orden,setFecha_Orden] = useState('')
    const[detalles_orden,setDetalles_Orden] = useState('')
    const[cantidad_producto,setCantidad_Producto] = useState('')
    const[id_clientes,setId_Clientes] = useState('')
    const[id_productos,setId_Productos] = useState('')
    const navegar = useNavigate()

    function pedidosInsertar()
    {
        const pedidoinsertar = {
        id: uniquid(),
        numero_orden: numero_orden,
        fecha_orden: fecha_orden,
        detalles_orden: detalles_orden,
        cantidad_producto: cantidad_producto,
        id_clientes: id_clientes,
        id_productos: id_productos
        }
    
        console.log(pedidoinsertar)

        axios.post(`/api/pedidos/agregar`,pedidoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/pedidoslistar')
        }).catch(err => {console.log(err.stack)})

    }

    function pedidosRegresar()
    {
        navegar('/pedidoslistar')
    }

return(
    <div className='container mt-5'>
        <h4>Pedidos</h4>
        <div className='row'>
            <div className='col-md-12'>
            <div className="mb-3">
                    <label htmlFor="numero_orden" className="form-label">Numero Orden</label>
                    <input type="text" className="form-control" id="numero_orden" value={numero_orden} onChange={(e)=>{setNumero_Orden(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="fecha_orden" className="form-label">Fecha Orden</label>
                    <input type="text" className="form-control" id="fecha_orden" value={fecha_orden} onChange={(e)=>{setFecha_Orden(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="detalles_orden" className="form-label">Detalles Orden</label>
                    <input type="text" className="form-control" id="detalles_orden" value={detalles_orden} onChange={(e)=>{setDetalles_Orden(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="cantidad_producto" className="form-label">Cantidad Producto</label>
                    <input type="text" className="form-control" id="cantidad_producto" value={cantidad_producto} onChange={(e)=>{setCantidad_Producto(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="id_clientes" className="form-label">Id Clientes</label>
                    <input type="text" className="form-control" id="id_clientes" value={id_clientes} onChange={(e)=>{setId_Clientes(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="id_productos" className="form-label">Id Producto</label>
                    <input type="text" className="form-control" id="id_productos" value={id_productos} onChange={(e)=>{setId_Productos(e.target.value)}}></input>
                </div>

                <button type="button" className="btn btn-info" onClick={pedidosRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={pedidosInsertar}>Actualizar</button>
            </div>
        </div>
    </div>
)

}

export default PedidosAgregar;