import axios from 'axios';
import uniquid from 'uniqid';
import React, {useState} from 'react';
import {useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function PagosAgregar()
{
    const[precio_total,setPrecio_Total] = useState('')
    const[id_cliente,setId_Cliente] = useState('')
    const[id_pedido,setId_Pedido] = useState('')
    
    const navegar = useNavigate()

    function pagosInsertar()
    {
        const pagoinsertar = {
        id: uniquid(),
        precio_total: precio_total,
        id_cliente: id_cliente,
        id_pedido:id_pedido
        }
    
        console.log(pagoinsertar)

        axios.post(`/api/pagos/agregar`,pagoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/pagoslistar')
        }).catch(err => {console.log(err.stack)})

    }

    function pagosRegresar()
    {
        navegar('/pagoslistar')
    }

return(
    <div className='container mt-5'>
        <h4>Pago</h4>
        <div className='row'>
            <div className='col-md-12'>
                <div className="mb-3">
                    <label htmlFor="precio_total" className="form-label">Precio_Total</label>
                    <input type="text" className="form-control" id="precio_total" value={precio_total} onChange={(e)=>{setPrecio_Total(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="id_cliente" className="form-label">Id_Cliente</label>
                    <input type="text" className="form-control" id="id_cliente" value={id_cliente} onChange={(e)=>{setId_Cliente(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="id_pedido" className="form-label">Id_Pedido</label>
                    <input type="text" className="form-control" id="id_pedido" value={id_pedido} onChange={(e)=>{setId_Pedido(e.target.value)}}></input>
                </div>
                

                <button type="button" className="btn btn-info" onClick={pagosRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={pagosInsertar}>Actualizar</button>
            </div>
        </div>
    </div>
)

}

export default PagosAgregar;