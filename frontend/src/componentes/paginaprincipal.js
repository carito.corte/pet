import React from 'react';

function PaginaPrincipal()
{

    return(
        <div className="container mt-5">
            <h4>Pagina Principal</h4>
            <div className="row">
                <div className="col-md-12">
                    
                </div>
            </div>
            <section class="padre">
                <p class="columna"><h1>Omega Pet Shop</h1> Somos una empresa para mascotas (perros y gatos) en la cual contamos con comida, jugutes, premios y productos de aseo para tu mascota para conocer nuestras categorias de click en el siguiente enlace<br></br><a href='/categoriaslistar'>Categorias</a></p>

                <p class="columna"><h1>Juguetes y premios</h1>Hay variedad de juguetes para mascotas como pelotas, huesos, Ovillos de lana, Rascadores para gato y demás juguetes, también tenemos premios, para conocer mas información de click en el siguiente enlace<br></br><a href='/productoslistar'>Productos</a></p>


                <p class="columna"><h1>Servicio de ayuda</h1>Se puede comunicar con nuestros trabajadores los cuales les indicaran y solucionaran cualquier incomveniente que se les presente en la pagina web dando en el siguientes link<br></br><a href='/empleadoslistar'>Empleados</a></p>

            </section>
        </div>
    )

}

export default PaginaPrincipal;