import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import PedidosBorrar from '../borrar/pedidosborrar';


function PedidosListar()
{
    const[dataPedidos, setdataPedidos] = useState([]);
    //Peticion GET para listar pedidos utilizando axios
    useEffect(()=>{axios.get('api/pedidos/listar').then(res => {
        console.log(res.data)
        setdataPedidos(res.data)
    }).catch(err=>{console.log(err)})
    },[]);

    return(
        <div className="container mt-5" align="center">
        <h4>Lista de Pedidos</h4>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        
                        <tr>
                            <td align="center">Id</td>
                            <td align="center">Numero Orden</td>
                            <td align="center">Fecha Orden</td>
                            <td align="center">Detalles Orden</td>
                            <td align="center">Cantidad Producto</td>
                            <td align="center">Id Cliente</td>
                            <td align="center">Id Productos</td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </thead> 
                    <tbody>
                    {dataPedidos.map(mipedido => (
                        <tr>
                            <td align="center">{mipedido.id}</td>
                            <td align="center">{mipedido.numero_orden}</td>
                            <td align="center">{mipedido.fecha_orden}</td>
                            <td align="center">{mipedido.detalles_orden}</td>
                            <td align="center">{mipedido.cantidad_producto}</td>
                            <td align="right">{mipedido.id_clientes}</td>
                            <td align="center">{mipedido.id_productos}</td>
                            <td align="center"><Link to={`/pedidoseditar/${mipedido.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{PedidosBorrar(mipedido.id)}}>Borrar</li></td>                                           
                        </tr>
                        
                    ))}
                    </tbody>
                    
                    <tr key={0}>
                            <td colSpan={6} align="right"><Link to={`/pedidosagregar`}><li className='btn btn-success'>Agregar Pedido</li></Link></td>
                        </tr>
                </table>
            </div>
        </div>
        <section className="mt-5 mb-5">
        <div align="center">
            Copyright (c) 2022 - MisionTIC
        </div> 
        </section>
        </div>
    )

}

export default PedidosListar;

