import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import EmpleadosBorrar from '../borrar/empleadosborrar';



function EmpleadosListar()
{
    const[dataEmpleados, setdataEmpleados] = useState([]);
    //Peticion GET para listar empleados utilizando axios
    useEffect(()=>{axios.get('api/empleados/listar').then(res => {
        console.log(res.data)
        setdataEmpleados(res.data)
    }).catch(err=>{console.log(err)})
    },[]);

    return(
        <div className="container mt-5" align="center">
        <h4>Lista de Empleados</h4>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        
                        <tr>
                            <td align="center">Id</td>
                            <td align="center">Nombre Empleado</td>
                            <td align="center">Apellido Empleado</td>
                            <td align="center">Cedula Empleado</td>
                            
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </thead> 
                    <tbody>
                    {dataEmpleados.map(miempleado => (
                        <tr>
                            <td align="center">{miempleado.id}</td>
                            <td align="center">{miempleado.nombre_empleado}</td>
                           
                            <td align="center">{miempleado.apellido_empleado}</td>
                            <td align="center">{miempleado.cedula_empleado}</td>
                            <td align="center"><Link to={`/empleadoseditar/${miempleado.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{EmpleadosBorrar(miempleado.id)}}>Borrar</li></td>                                           
                        </tr>
                        
                    ))}
                    </tbody>
                    
                    <tr key={0}>
                            <td colSpan={6} align="right"><Link to={`/empleadosagregar`}><li className='btn btn-success'>Agregar Empleado</li></Link></td>
                        </tr>
                </table>
            </div>
        </div>
        <section className="mt-5 mb-5">
        <div align="center">
            Copyright (c) 2022 - MisionTIC
        </div> 
        </section>
        </div>
    )

}

export default EmpleadosListar;

