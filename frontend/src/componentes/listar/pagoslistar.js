import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import PagosBorrar from '../borrar/pagosborrar';


function PagosListar()
{
    const[dataPagos, setdataPagos] = useState([]);
    //Peticion GET para listar pagos utilizando axios
    useEffect(()=>{axios.get('api/pagos/listar').then(res => {
        console.log(res.data)
        setdataPagos(res.data)
    }).catch(err=>{console.log(err)})
    },[]);

    return(
        <div className="container mt-5" align="center">
        <h4>Lista de Pagos</h4>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        
                        <tr>
                            <td align="center">Id</td>
                            <td align="center">Pecio Total</td>
                            <td align="center">Id Cliente</td>
                            <td align="center">Id Pedido</td>
                           
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </thead> 
                    <tbody>
                    {dataPagos.map(mipago => (
                        <tr>
                            <td align="center">{mipago.id}</td>
                            <td align="center">{mipago.precio_total}</td>
                            <td align='center'>{mipago.id_cliente}</td>
                            <td align="center">{mipago.id_pedido}</td>
                            <td align="center"><Link to={`/pagoseditar/${mipago.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{PagosBorrar(mipago.id)}}>Borrar</li></td>                                           
                        </tr>
                        
                    ))}
                    </tbody>
                    
                    <tr key={0}>
                            <td colSpan={6} align="right"><Link to={`/pagosagregar`}><li className='btn btn-success'>Agregar Pago</li></Link></td>
                        </tr>
                </table>
            </div>
        </div>
        <section className="mt-5 mb-5">
        <div align="center">
            Copyright (c) 2022 - MisionTIC
        </div> 
        </section>
        </div>
    )

}

export default PagosListar;

