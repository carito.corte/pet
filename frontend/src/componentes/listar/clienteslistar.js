import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import ClientesBorrar from '../borrar/clientesborrar';

function ClientesListar()
{
    const[dataClientes, setdataClientes] = useState([]);
    //Peticion GET para listar clientes utilizando axios
    useEffect(()=>{axios.get('api/clientes/listar').then(res => {
        console.log(res.data)
        setdataClientes(res.data)
    }).catch(err=>{console.log(err)})
    },[]);

    return(
        <div className="container mt-5" align="center">
        <h4>Lista de Clientes</h4>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        
                        <tr>
                            <td align="center">Id</td>
                            <td align="center">Nombre</td>
                            <td align="center">Apellido</td>
                            <td align="center">Numero Cliente</td>
                            <td align="center">Direccion Cliente</td>
                            <td align="center">id Empleado</td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </thead> 
                    <tbody>
                    {dataClientes.map(micliente => (
                        <tr>
                            <td align="center">{micliente.id}</td>
                            <td align="center">{micliente.nombre}</td>
                            <td align="center">{micliente.apellido}</td>
                            <td align="center">{micliente.numero_cliente}</td>
                            <td align="right">{micliente.direccion_cliente}</td>
                            <td align="center">{micliente.id_empleados}</td>
                            <td align="center"><Link to={`/clienteseditar/${micliente.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{ClientesBorrar(micliente.id)}}>Borrar</li></td>                                           
                        </tr>
                        
                    ))}
                    </tbody>
                    
                    <tr key={0}>
                            <td colSpan={6} align="right"><Link to={`/clientesagregar`}><li className='btn btn-success'>Agregar Cliente</li></Link></td>
                        </tr>
                </table>
            </div>
        </div>
        <section className="mt-5 mb-5">
        <div align="center">
            Copyright (c) 2022 - MisionTIC
        </div> 
        </section>
        </div>
    )

}

export default ClientesListar;
