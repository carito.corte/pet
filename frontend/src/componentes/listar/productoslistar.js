import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import ProductosBorrar from '../borrar/productosborrar';

//import ProductosDetalle from '../componentes/productosdetalle';
//const navegar = useNavigate();
//navegar(0)

function ProductosListar()
{
    const[dataProductos, setdataProductos] = useState([]);
    //Peticion GET para listar productos utilizando axios
    useEffect(()=>{axios.get('api/productos/listar').then(res => {
        console.log(res.data)
        setdataProductos(res.data)
    }).catch(err=>{console.log(err)})
    },[]);

    return(
        <div className="container mt-5" align="center">
        <h4>Lista de Productos</h4>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        
                        <tr>
                            <td align="center">Id</td>
                            <td align="center">Id Categorias</td>
                            <td align="center">Nombre</td>
                            <td align="center">Descripcion</td>
                            <td align="center">Cantidad</td>
                            <td align="center">Precio</td>
                            <td align="center">Activo</td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </thead> 
                    <tbody>
                    {dataProductos.map(miproducto => (
                        <tr>
                            <td align="center">{miproducto.id}</td>
                            <td align="center">{miproducto.id_categoria}</td>
                            <td>{miproducto.nombre}</td>
                            <td align="center">{miproducto.descripcion}</td>
                            <td align="center">{miproducto.cantidad}</td>
                            <td align="right">{miproducto.precio}</td>
                            <td align="center">{miproducto.activo ? 'Activo' : 'Inactivo'}</td>
                            <td align="center"><Link to={`/productoseditar/${miproducto.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{ProductosBorrar(miproducto.id)}}>Borrar</li></td>                                           
                        </tr>
                        
                    ))}
                    </tbody>
                    
                    <tr key={0}>
                            <td colSpan={6} align="right"><Link to={`/productosagregar`}><li className='btn btn-success'>Agregar Producto</li></Link></td>
                        </tr>
                </table>
            </div>
        </div>
        <section className="mt-5 mb-5">
        <div align="center">
            Copyright (c) 2022 - MisionTIC
        </div> 
        </section>
        </div>
    )

}
/*
function productosBorrar(id_borrar)
{

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: false
        }).then((result) => {
        if (result.isConfirmed) {
        //axios.delete('/api/productos/borrar',{id:id_borrar}).then(res=> {
        axios.delete(`/api/productos/borrar/${id_borrar}`).then(res=> {
            console.log(res.data)
            ProductosListar();
        }).catch(err=>{console.log(err)})
        swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
        )
        } else if (
        result.dismiss === Swal.DismissReason.cancel
        ) {
        swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
            )
        }
    })
}
*/

//ENVIO DE PARAMETROS AL COMPONENTE
/*

const listaproductos = dataProductos.map(miproducto => {return(
    <div>
        <ProductosDetalle miproducto={miproducto}/>
    </div>
)
})
return(
        <div>
            <h1>Lista de Productos</h1>
            {listaproductos}
        </div>
    )
*/

//<td><img src={`${process.env.PUBLIC_URL}/imagenes/${miproducto.img}`} alt={miproducto.nombre} width="30px" className="img-fluid"/></td>

export default ProductosListar;

