import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import CategoriasBorrar from '../borrar/categoriasborrar';

function CategoriasListar()
{
    const[dataCategorias, setdataCategorias] = useState([]);
    //Peticion GET para listar categorias utilizando axios
    useEffect(()=>{axios.get('api/categorias/listar').then(res => {
        console.log(res.data)
        setdataCategorias(res.data)
    }).catch(err=>{console.log(err)})
    },[]);

    return(
        <div className="container mt-5" align="center">
        <h4>Lista de Categorias</h4>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        
                        <tr>
                            <td align="center">Id</td>
                            <td align="center">Descripcion</td>
                            <td align="center">Imagen</td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </thead> 
                    <tbody>
                    {dataCategorias.map(micategoria => (
                        <tr>
                            <td align="center">{micategoria.id}</td>
                            <td align="center">{micategoria.descripcion}</td>
                            <td align="center">{micategoria.imagen}</td>
                            <td align="center"><Link to={`/categoriaseditar/${micategoria.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{CategoriasBorrar(micategoria.id)}}>Borrar</li></td>                                           
                        </tr>
                    ))}
                    </tbody>
                    <tr key={0}>
                            <td colSpan={6} align="right"><Link to={`/categoriasagregar`}><li className='btn btn-success'>Agregar Producto</li></Link></td>
                        </tr>
                </table>
            </div>
        </div>
        <section className="mt-5 mb-5">
        <div align="center">
            Copyright (c) 2022 - MisionTIC
        </div> 
        </section>
        </div>
    )

}

export default CategoriasListar;

