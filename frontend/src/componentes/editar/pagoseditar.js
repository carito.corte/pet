import axios from 'axios';
import React, {useEffect, useState} from 'react';
import { useParams, useNavigate } from 'react-router';
import Swal from 'sweetalert2';

function PagosEditar()
{
    const parametros = useParams()

    const[precio_total,setPrecio_Total] = useState('')
    const[id_cliente,setId_Cliente] = useState('')
    const[id_pedido,setId_Pedido] = useState('')
    
    const navegar = useNavigate()

    useEffect(()=>{axios.get(`/api/pagos/cargar/${parametros.id}`).then(res=>{
        console.log(res.data[0])
        const dataPagos = res.data[0]
        setPrecio_Total(dataPagos.precio_total)
        setId_Cliente(dataPagos.id_cliente)
        setId_Pedido(dataPagos.id_pedido)
        
    })},[parametros.id])

    function pagosActualizar()
    {
        const pagoeditar = {
        id: parametros.id,
        precio_total: precio_total,
        id_cliente: id_cliente,
        id_pedido: id_pedido,
        
        }
    
        console.log(pagoeditar)

        axios.post(`/api/pagos/editar/${parametros.id}`,pagoeditar).then(res=> {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/pagoslistar')
        }).catch(err => {console.log(err.stack)})

    }

    function pagosRegresar()
    {
        navegar('/pagoslistar')
    }

return(
    <div className='container mt-5'>
        <h4>Pagos</h4>
        <div className='row'>
            <div className='col-md-12'>
                <div className="mb-3">
                    <label htmlFor="precio_total" className="form-label">precio_total</label>
                    <input type="text" className="form-control" id="precio_total" value={precio_total} onChange={(e)=>{setPrecio_Total(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="id_cliente" className="form-label">Id_Cliente</label>
                    <input type="text" className="form-control" id="id_cliente" value={id_cliente} onChange={(e)=>{setId_Cliente(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="id_pedido" className="form-label">Id_Pedido</label>
                    <input type="text" className="form-control" id="id_pedido" value={id_pedido} onChange={(e)=>{setId_Pedido(e.target.value)}}></input>
                </div>
                
                <button type="button" className="btn btn-info" onClick={pagosRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={pagosActualizar}>Actualizar</button>
            </div>
        </div>
    </div>
)

}



export default PagosEditar;