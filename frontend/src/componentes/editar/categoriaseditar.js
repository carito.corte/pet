import axios from 'axios';
import React, {useEffect, useState} from 'react';
import { useParams, useNavigate } from 'react-router';
import Swal from 'sweetalert2';

function CategoriasEditar()
{
    const parametros = useParams()

    const[descripcion,setDescripcion] = useState('')
    const[imagen,setImagen] = useState('')
    const navegar = useNavigate()

    useEffect(()=>{axios.get(`/api/categorias/cargar/${parametros.id}`).then(res=>{
        console.log(res.data[0])
        const dataCategorias = res.data[0]
        setDescripcion(dataCategorias.descripcion)
        setImagen(dataCategorias.imagen)
        
    })},[parametros.id])

    function categoriasActualizar()
    {
        const categoriaeditar = {
        id: parametros.id,
        descripcion: descripcion,
        imagen: imagen
        }
    
        console.log(categoriaeditar)

        axios.post(`/api/categorias/editar/${parametros.id}`,categoriaeditar).then(res=> {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/categoriaslistar')
        }).catch(err => {console.log(err.stack)})

    }

    function categoriasRegresar()
    {
        navegar('/categoriaslistar')
    }

return(
    <div className='container mt-5'>
        <h4>Categoria</h4>
        <div className='row'>
            <div className='col-md-12'>
                <div className="mb-3">
                    <label htmlFor="descripcion" className="form-label">Descripcion</label>
                    <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e)=>{setDescripcion(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="imagen" className="form-label">Imagen</label>
                    <input type="text" className="form-control" id="imagen" value={imagen} onChange={(e)=>{setImagen(e.target.value)}}></input>
                </div>
                

                <button type="button" className="btn btn-info" onClick={categoriasRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={categoriasActualizar}>Actualizar</button>
            </div>
        </div>
    </div>
)

}



export default CategoriasEditar;