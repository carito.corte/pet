import axios from 'axios';
import React, {useEffect, useState} from 'react';
import { useParams, useNavigate } from 'react-router';
import Swal from 'sweetalert2';

function ClientesEditar()
{
    const parametros = useParams()

    const[nombre,setNombre] = useState('')
    const[apellido,setApellido] = useState('')
    const[numero_cliente,setNumero_Cliente] = useState('')
    const[direccion_cliente,setDireccion_Cliente] = useState('')
    const[id_empleados,setId_Empleados] = useState('')
    const navegar = useNavigate()

    useEffect(()=>{axios.get(`/api/clientes/cargar/${parametros.id}`).then(res=>{
        console.log(res.data[0])
        const dataClientes = res.data[0]
        setNombre(dataClientes.nombre)
        setApellido(dataClientes.apellido)
        setNumero_Cliente(dataClientes.numero_cliente)
        setDireccion_Cliente(dataClientes.direccion_cliente)
        setId_Empleados(dataClientes.id_empleados)
    })},[parametros.id])

    function clientesActualizar()
    {
        const clienteeditar = {
        id: parametros.id,
        nombre: nombre,
        apellido: apellido,
        numero_cliente: numero_cliente,
        direccion_cliente: direccion_cliente,
        id_empleados: id_empleados
        }
    
        console.log(clienteeditar)

        axios.post(`/api/clientes/editar/${parametros.id}`,clienteeditar).then(res=> {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/clienteslistar')
        }).catch(err => {console.log(err.stack)})

    }

    function clientesRegresar()
    {
        navegar('/clienteslistar')
    }

return(
    <div className='container mt-5'>
        <h4>Clientes</h4>
        <div className='row'>
            <div className='col-md-12'>
                
                <div className="mb-3">
                    <label htmlFor="nombre" className="form-label">Nombre</label>
                    <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="apellido" className="form-label">Apellido</label>
                    <input type="text" className="form-control" id="apellido" value={apellido} onChange={(e)=>{setApellido(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="numero_cliente" className="form-label">Numero Cliente</label>
                    <input type="text" className="form-control" id="numero_cliente" value={numero_cliente} onChange={(e)=>{setNumero_Cliente(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="direccion_cliente" className="form-label">Direccion Cliente</label>
                    <input type="text" className="form-control" id="direccion_cliente" value={direccion_cliente} onChange={(e)=>{setDireccion_Cliente(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="id_empleado" className="form-label">Id_Empleados</label>
                    <input type="text" className="form-control" id="id_empleado" value={id_empleados} onChange={(e)=>{setId_Empleados(e.target.value)}}></input>
                </div>

                <button type="button" className="btn btn-info" onClick={clientesRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={clientesActualizar}>Actualizar</button>
            </div>
        </div>
    </div>
)

}



export default ClientesEditar;